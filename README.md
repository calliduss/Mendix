* Programming language: Java
* Test Framework: TestNG
* Framework to test UI: Selenium
* Framework to test API: Rest Assured
* Docker container: https://hub.docker.com/r/markhobson/maven-chrome
* CI/CD: Gitlab
* Gitlab pipelines: https://gitlab.com/calliduss/Mendix/-/pipelines
* Gitlab schedule: https://gitlab.com/calliduss/Mendix/-/pipeline_schedules
* Browser: Chrome

Test runs in headless mode