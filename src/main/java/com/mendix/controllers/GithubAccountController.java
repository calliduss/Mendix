package com.mendix.controllers;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static com.mendix.Constants.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class GithubAccountController {

    private final RequestSpecification requestSpecification;
    private final ResponseSpecification responseSpecification;

    public GithubAccountController() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri(BASE_API_URL);
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecification = requestSpecBuilder.build();

        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
        responseSpecBuilder.expectStatusCode(anyOf(is(both(greaterThanOrEqualTo(200)).and(lessThan(300)))));
        responseSpecBuilder.expectContentType(ContentType.JSON);
        responseSpecBuilder.log(LogDetail.ALL);
        responseSpecification = responseSpecBuilder.build();
    }

    public int getNumberOfAccountPublicRepos(String account) {
        Response response = given(requestSpecification)
                .get(USERS_ENDPOINT + "/" + account);

        response.then().spec(responseSpecification);

        return response.then().extract().path("public_repos");
    }
}
