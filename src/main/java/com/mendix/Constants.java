package com.mendix;

import java.io.File;

public class Constants {
    public static final String BASE_URL = "https://www.github.com/";
    public static final String BASE_API_URL = "https://api.github.com";
    public static final String USERS_ENDPOINT = BASE_API_URL + "/users";
    public static final String LOCAL_ENVIRONMENT_PATH = System.getProperty("user.dir");
    public static final String PATH_TO_LOCAL_WINDOWS_CHROMEDRIVER = LOCAL_ENVIRONMENT_PATH + File.separator + "src"
            + File.separator + "main"
            + File.separator + "resources"
            + File.separator + "chromedriver"
            + File.separator + "windows"
            + File.separator + "chromedriver.exe";
}