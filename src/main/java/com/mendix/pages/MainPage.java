package com.mendix.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class MainPage extends BasePageObject {

    private final By searchField = By.xpath("//*[@data-test-selector='nav-search-input']");

    public MainPage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public SearchResultsPage searchOnGithub(String value) {
        log.info("Enter the value in the search field");
        driver.findElement(searchField).clear();
        driver.findElement(searchField).sendKeys(value);
        driver.findElement(searchField).sendKeys(Keys.ENTER);

        return new SearchResultsPage(driver, log);
    }
}