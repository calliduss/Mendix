package com.mendix.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProfilePage extends BasePageObject {

    private final By repositoriesCounter = By.xpath("(//*[contains(@href,'tab=repositories')])[1]//*[@class='Counter']");

    public ProfilePage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public int getRepositoriesQuantity() {
        log.info("Get the number of public repositories on the <Repositories> tab");
        var quantity = driver.findElement(repositoriesCounter).getText();
        return Integer.parseInt(quantity);
    }
}