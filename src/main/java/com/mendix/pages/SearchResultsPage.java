package com.mendix.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultsPage extends BasePageObject {

    private final By usersMenuItem = By.xpath("(//*[@data-search-type='Users'])[1]/parent::*[@class='menu-item']");
    private String accountInSearchResults = "//*[@id='user_search_results']//em[text()='*#*']";

    public SearchResultsPage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public ProfilePage findAndOpenResultItemByAccountName(String accountName) {
        log.info("Find and open the result in the list by account name");
        driver.findElement(usersMenuItem).click();
        driver.findElement(By.xpath(accountInSearchResults.replace("*#*", accountName))).click();

        return new ProfilePage(driver, log);
    }
}
