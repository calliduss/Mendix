package com.mendix.base;

import com.mendix.controllers.GithubAccountController;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import static com.mendix.Constants.BASE_URL;
import static io.restassured.config.EncoderConfig.encoderConfig;

public class BaseTest {

    public static GithubAccountController githubAccountController;
    protected ChromeDriver driver;
    protected Logger log;

    @BeforeMethod
    public void setUp() {
        ChromeOptions options = new ChromeOptions().setHeadless(true);
        options.addArguments("--disable-gpu");
        options.addArguments("--disable-extensions");
        options.addArguments("--window-size=1920,1080");
        options.addArguments("start-maximized");
        options.addArguments("disable-notifications");
        options.addArguments("allow-running-insecure-content");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--no-sandbox");

//        System.setProperty("webdriver.chrome.driver", PATH_TO_LOCAL_WINDOWS_CHROMEDRIVER);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        log = LogManager.getLogger();

        RestAssured.config = new RestAssuredConfig()
                .encoderConfig(encoderConfig()
                        .defaultContentCharset("UTF-8"));
        githubAccountController = new GithubAccountController();

        driver.get(BASE_URL);
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(Method method) {
        log.info("End of " + method.getName() + " test");
        log.info("Close driver");
        driver.quit();
    }
}
