import com.mendix.base.BaseTest;
import com.mendix.pages.MainPage;
import com.mendix.pages.SearchResultsPage;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RepositoryTests extends BaseTest {

    @Test
    public void public_repos_should_be_equal_both_in_interface_and_api() {
        var mainPage = new MainPage(driver, log);
        var searchResultsPage = new SearchResultsPage(driver, log);
        var account = "calliduss";
        int publicReposApiCount = githubAccountController.getNumberOfAccountPublicRepos(account);

        mainPage.searchOnGithub(account);
        var publicReposUICount = searchResultsPage.findAndOpenResultItemByAccountName(account).getRepositoriesQuantity();

        assertThat(publicReposApiCount, equalTo(publicReposUICount));
    }
}